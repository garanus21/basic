<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220112192225 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `registrations` (id INT AUTO_INCREMENT NOT NULL, status INT NOT NULL, name VARCHAR(255) NOT NULL, street VARCHAR(255) NOT NULL, number VARCHAR(20) NOT NULL, town VARCHAR(255) NOT NULL, postcode VARCHAR(6) NOT NULL, province VARCHAR(255) NOT NULL, directional VARCHAR(4) NOT NULL, phone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, identifier INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE `registrations`');
    }
}
