<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Register;
use App\Services\ProvincesService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Class RegisterType
 *
 * @package App\Form\Type
 */
class RegisterType extends AbstractType
{
    private $provinces;

    public function __construct(
        ProvincesService $provincesService
    ) {
        $this->provinces = $provincesService;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class,[
                'choices'  => [
                    'Klient indywidualny' => 0,
                    'Firma' => 1,
                ],
            ])
            ->add('name', TextType::class, [
                'attr' => ['class' => 'form-control']
                ])
            ->add('street', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('number', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('town', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('postcode', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('province', ChoiceType::class, [
                'choices' => $this->provinces->provincesList(),
                'attr' => ['class' => 'form-control'],
            ])
            ->add('directional', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => '+48'
                ]
            ])
            ->add('phone', TelType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('email', EmailType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('identifier', NumberType::class, [
                'attr' => ['class' => 'form-control'],
                'constraints' => [new Length(['min' => 11, 'max' => 255])],
            ])
            ->add('save', SubmitType::class,[
                'attr' => ['class' => 'btn btn-success'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Register::class,
        ]);
    }
}
