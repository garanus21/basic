<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Register;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class RegisterRepository
 *
 * Register repository.
 *
 * @package App\Repository
 */
class RegisterRepository extends ServiceEntityRepository
{
    /**
     * RegisterRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Register::class);
    }
}
