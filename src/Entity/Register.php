<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\RegisterRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class Register
 *
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass=RegisterRepository::class)
 * @ORM\Table(name="`registrations`")
 */
class Register
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Status (eg. 0 - user, 1 - company).
     *
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $status;

    /**
     * Name registry user/company.
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * Street.
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $street;

    /**
     * Number of the building.
     *
     * @var string
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    private $number;

    /**
     * Town.
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $town;

    /**
     * Postcode.
     *
     * @var string
     * @ORM\Column(type="string", length=6, nullable=false)
     */
    private $postcode;

    /**
     * Province.
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $province;

    /**
     * Directional.
     *
     * @var string
     * @ORM\Column(type="string", length=4, nullable=false)
     */
    private $directional;

    /**
     * Phone number.
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $phone;

    /**
     * E-mail.
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * Identifier.
     *
     * @var int
     * @ORM\Column(type="bigint", nullable=false)
     */
    private $identifier;

    /**
     * @return mixed|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string|null
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    /**
     * @return string|null
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @param string $town
     */
    public function setTown(string $town): void
    {
        $this->town = $town;
    }

    /**
     * @return string|null
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode(string $postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string|null
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param string $province
     */
    public function setProvince(string $province): void
    {
        $this->province = $province;
    }

    /**
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int|null
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param int $identifier
     */
    public function setIdentifier(int $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return string|null
     */
    public function getDirectional()
    {
        return $this->directional;
    }

    /**
     * @param string $directional
     */
    public function setDirectional(string $directional): void
    {
        $this->directional = $directional;
    }
}
