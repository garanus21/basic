<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Register;
use App\Form\Type\RegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 *
 * @package App\Controller
 */
class AdminController extends AbstractController
{
    /**
     * Admin index view.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $registrations = $this->getDoctrine()->getRepository(Register::class)->findAll();
        return $this->render('admin/index.html.twig',[
            'registrations' => $registrations
        ]);
    }

    /**
     * Show client.
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showClient(int $id)
    {
        $registration = $this->getDoctrine()->getRepository(Register::class)->find($id);
        return $this->render('admin/show_client.html.twig',[
            'registration' => $registration
        ]);
    }

    /**
     * Edit client details.
     *
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editClient(Request $request,int $id)
    {
        $register = $this->getDoctrine()->getRepository(Register::class)->find($id);
        $form = $this->createForm(RegisterType::class, $register);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $register = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($register);
            $entityManager->flush();

            return $this->redirectToRoute('admin_index');
        }
        return $this->render('register/register.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * Delete client.
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteClient(int $id)
    {
        $registration = $this->getDoctrine()->getRepository(Register::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($registration);
        $entityManager->flush();

        return $this->redirectToRoute('admin_index');
    }
}
