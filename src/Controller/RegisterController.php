<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Register;
use App\Form\Type\RegisterType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class RegisterController
 *
 * @package App\Controller
 */
class RegisterController extends AbstractController
{
    /**
     * Register form view.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $register = new Register();
        $form = $this->createForm(RegisterType::class, $register);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $register = $form->getData();
             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($register);
             $entityManager->flush();

            return $this->redirectToRoute('admin_index');
        }
        return $this->render('register/register.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
