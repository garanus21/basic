<?php

namespace App\Controller;

use App\Entity\Register;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class IndexController
 *
 * @package App\Controller
 */
class IndexController extends AbstractController
{
    /**
     * Home page index view.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('home/home.html.twig');
    }

    /**
     * Show client form details.
     *
     * @param int $id
     * @return object|null
     */
    public function clientShow(int $id)
    {
        $client = $this->getDoctrine()->getRepository(Register::class)->find($id);

        return $this->render('home/client.html.twig', [
            'client' => $client
        ]);
    }

    /**
     * Show client from details as JSON Response.
     *
     * @param int $id
     * @return Response
     */
    public function clientJson(int $id)
    {
        $client = $this->getDoctrine()->getRepository(Register::class)->find($id);

        $serializer = $this->get('serializer');
        $response = $serializer->serialize($client,'json');
        $response = new Response($response);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
