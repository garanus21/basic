<?php

declare(strict_types=1);

namespace App\Services;

/**
 * Class ProvincesService
 *
 * @package App\Services
 */
class ProvincesService
{
    /**
     * Getting array with provinces.
     *
     * @return array|bool|false|mixed|string
     */
    public function provincesList()
    {
        $url = "http://api.dro.nazwa.pl/";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        $resp=json_decode($resp);
        $resp = array_combine($resp, $resp);

        return $resp;
    }

}
